import functools as f
from multiprocessing import Pool
import time

testinput = "Fear no more the heat of the sun. Thy work is done; thy race is won. Golden lads and girls all must, as chimney sweeps, come to dust. Lads. Girls. Lads and Girls"

def findGeneral(word, string):
	if word.upper() in string.upper():
		return True
	else:
		return False

findLad = f.partial(findGeneral, "Lad")
findGirl = f.partial(findGeneral, "Girl")

splitInput = testinput.split(".")


#More than one way to do things - find independently & then combine...
toKeepLad = set(filter(findLad , splitInput))
toKeepGirl = set(filter(findGirl , splitInput))
toKeep = toKeepLad.intersection(toKeepGirl)

#Or do it together, but needs a lambda to make it run correctly
toKeepJoint = list(filter(lambda x: findGirl(x) and findLad(x), splitInput))

#print(toKeepLad)
#print(toKeepGirl)
#print(toKeep)
#print(toKeepJoint)

print("###########################################################")
###########################
#And now, in parallel:

#First we tweak the function to return the sentence:
def keepSentence(x):
    if (findLad(x) and findGirl(x)):
        return x

#MP relies on main as a condition; errors otherwise
if __name__ == '__main__':

#Load and flatten the input
    bigInput = []
    with open("C:/Users/mhw/mw/Software/CompMed/FPCode/cymbeline.txt") as text:
        for line in text:
            bigInput.append(line.split("."))

    flatInput = [item for sublist in bigInput for item in sublist]

    t0 = time.time()
    toKeepJoint = list(filter(lambda x: findGirl(x) and findLad(x), flatInput))
    t1 = time.time()
    print("TKJ: ", t1-t0, "\n", toKeepJoint)

## Make the Pool of workers
    p = Pool(4)
# and return the results
    t2 = time.time()
    results = p.map(keepSentence, flatInput)

#close the pool and wait for the work to finish
    p.close()
    p.join()
    #Because we used Map, lots of None
    cleanRes = [res for res in results if res !=None]
    t3 = time.time()

    print("Multi-thread: ", t3-t2, "\n")

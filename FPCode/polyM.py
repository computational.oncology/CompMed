#polyM.py

in1 = 1; in2 = 2
in3 = 2.3; in4 = 3.5
in5 = (1,2); in6 = (10,10)

def myMean(x,y):
    if type(x) != type(y):
        raise TypeError
    if type(x) != tuple:
        return (x +y)/2
    else:
        meanX = (x[0] + y[0])/2
        meanY = (x[1] + y[1])/2
        return (meanX, meanY)

print(myMean(in1, in2))
print(myMean(in3, in4))
print(myMean(in5, in6))

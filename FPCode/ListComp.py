import types
num = [1,2,3,4]

squares = [x**2 for x in num]
print(squares)

pairs = [(x, y) for x in num for y in num if x != y]
print(pairs)

a_list = [1, '4', 9, 'a', 0, 4]

squared_ints = [ e**2 for e in a_list if type(e) == type(1)]

print(squared_ints)
# [ 1, 81, 0, 16 ]

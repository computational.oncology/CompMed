from functools import partial

def myExp(x,y):
    return x**y

square = partial(myExp, y = 2)
cube = partial(myExp, y = 3)
quad = partial(myExp, y = 4)

print(square(191))
print(cube(3))
print(quad(10))
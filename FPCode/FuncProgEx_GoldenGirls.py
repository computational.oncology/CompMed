import functools as f
from multiprocessing import Pool

testinput = "Fear no more the heat of the sun. Thy work is done; thy race is won. Golden lads and girls all must, as chimney sweeps, come to dust. Lads. Girls. Lads and Girls"

def findGeneral(word, string):
	if word.upper() in string.upper():
		return True
	else:
		return False

findLad = f.partial(findGeneral, "Lad")
findGirl = f.partial(findGeneral, "Girl")

splitInput = testinput.split(".")


#This is just for interest
#It doesn't run correctly
Find1 = list(map((findLad and findGirl), splitInput))

#More than one way to do things - find independently & then combine...
toKeepLad = set(filter(findLad , splitInput))
toKeepGirl = set(filter(findGirl , splitInput))
toKeep = toKeepLad.intersection(toKeepGirl)

#Or do it together, but needs a lambda to make it run correctly
toKeepJoint = list(filter(lambda x: findGirl(x) and findLad(x), splitInput))

#Or make it a function
def keepSentence(x):
    if (findLad(x) and findGirl(x)):
        return x

#print(toKeepLad)
#print(toKeepGirl)
print(toKeep)
print(toKeepJoint)

#sen = keepSentence(testinput)
#print(sen)

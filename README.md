
This is a public repo for the CompMed Course

It WILL be wiped after use - so don't leave anything important here!

Suggested tasks:

1. Clone the existing repo
2. Open an existing file (e.g. ClassAnimals01.py). Then save that file as
XXClassAnimals01.py, where XX are your initials
3. Add the new file, using git add XXClassAnimals01.py locally
4. Commit it locally
4. Push it back to the repo

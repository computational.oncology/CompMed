from geopy import geocoders
import gmplot

key = "YOURAPIKEYHERE"
g= geocoders.GoogleV3(api_key=key)
inputAddress = 'Chrisp Street Health Centre, 100 Chrisp St, Poplar, London E14 6PG'
location = g.geocode(inputAddress, timeout=10)

print(location.latitude, location.longitude)
print(location.raw)
print(location.address)

gmap = gmplot.GoogleMapPlotter(location.latitude, location.longitude, 16)
gmap.draw("mymap.html")

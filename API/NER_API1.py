import ner
tagger = ner.HttpNER(host='localhost', port=8080)
test1 = "'A second EU referendum would divide the UK again but Labour would not rule out further �democratic engagement�, John McDonnell has said, ahead of a series of speeches by cabinet ministers on Britain�s future after Brexit. The shadow chancellor said the country was still smarting from the aftermath of the 2016 vote and he was doubtful another referendum would bring the country together."
test2 = " �Those divisions are really still there,� he told ITV�s Peston on Sunday, saying he was concerned about prompting rightwing xenophobia. Asked whether Labour would rule out a second referendum, he said: �We�d never turn our back on democratic engagement. �I think better we have a general election. Better we have a general election. On the issue, and all the other issues, because you then have a wider debate as well.� Earlier on Sunday, the Conservative MP Anna Soubry appeared alongside Labour�s Chuka Umunna on the BBC�s The Andrew Marr Show, where she was asked whether pro-EU Tories might defeat �the kind of Brexit the prime minister wants�. �If she�s not careful, yes,� Soubry said. Asked whether MPs might stop Brexit altogether, she said: �We won�t stop it. It is the people. We gave the people a referendum to start this process."
res1 = tagger.get_entities(test1)
res2 = tagger.get_entities(test2)
print(res1)
print(res2)

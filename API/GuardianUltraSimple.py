#GuardianUltraSimple.py
import urllib.request
import json

URL = "https://content.guardianapis.com/search?q=debate&tag=politics/politics&from-date=2018-01-01&api-key=test"

#Encoding issues - so convert to UTF-8
res = urllib.request.urlopen(URL).read().decode('utf8')

data = json.loads(res)
print(data)

for key in data['response']['results']:
    print(key, "\n")

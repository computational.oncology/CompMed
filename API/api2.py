import gmplot
from geopy import geocoders

#Connect to API
key = "YOURAPIKEYHERE"
g = geocoders.GoogleV3(api_key=key)

# Initialize two empty lists to hold the latitude and longitude values
latitude = []
longitude = []

#Input addresses
inputAddress = 'Chrisp Street Health Centre, 100 Chrisp St, Poplar, London E14 6PG'
inputAddress2 = 'Globe Town Surgery, 82-86 Roman Rd, London E2 0PJ'

#Find lat & long for all the addresses
addresses = [inputAddress,inputAddress2]
for i in addresses:
    location = g.geocode(i, timeout=10)
    latitude.append(location.latitude)
    longitude.append(location.longitude)

print(latitude)
print(longitude)

# Initialize the map to the first location in the list
gmap = gmplot.GoogleMapPlotter(latitude[0],longitude[0],13)

# Draw the points on the map. I used maker r'.
gmap.scatter(latitude, longitude, 'r', edge_width=10)

# Write the map in an HTML file
gmap.draw('map2.html')

#Geopy API example
#based on their docs
from geopy.geocoders import Nominatim
geolocator = Nominatim()
location = geolocator.geocode("Chrisp Street Health Centre, London")
print(location.address)
print((location.latitude, location.longitude))
print(location.raw)

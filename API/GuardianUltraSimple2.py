#GuardianUltraSimple.py
import urllib.request
import json

URL = "https://content.guardianapis.com/search?q=debate&tag=politics/politics&from-date=2018-01-01&api-key=test"
URL2 = "https://content.guardianapis.com/search?q=show-blocks=all&tag=politics/politics&from-date=2018-01-01&api-key=test"
URL3 = "https://content.guardianapis.com/search?q=debate&tag=politics/politics&show-fields=bodyText&from-date=2018-01-01&api-key=test"
#Encoding issues - so convert to UTF-8
res = urllib.request.urlopen(URL3).read().decode('utf8')

data = json.loads(res)
print(data)

for res in data['response']['results']:
    print(res.keys())
    print(res['fields'], "\n")
    #print(res, "\n")
    #print(res.keys(), "\n")

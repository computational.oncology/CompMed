import gmplot
from geopy import geocoders
import csv

#Connect to API
key = "YOURAPIKEYHERE"
g = geocoders.GoogleV3(api_key=key)

#Create empty address list
gp_address = []

#open csv file and read in data
infile = open("gp.csv", "r")
data = list(csv.reader(infile, delimiter=","))
for line in data:
    gp_address.append(line)
infile.close()

# Initialize two empty lists to hold the latitude and longitude values
latitude = []
longitude = []

#Find lat & long for all the addresses
for i in gp_address:
    location = g.geocode(i, timeout=10)
    latitude.append(location.latitude)
    longitude.append(location.longitude)

print(latitude)
print(longitude)

# Initialize the map to the first location in the list
gmap = gmplot.GoogleMapPlotter(latitude[0],longitude[0],13)

# Draw the points on the map. I used maker r'.
gmap.scatter(latitude, longitude, 'r', edge_width=10)

# Write the map in an HTML file
gmap.draw('map3.html')

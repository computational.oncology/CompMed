"""A file to demonstrate some simple class syntax and usage"""
class MyClass(object):
    """A simple class"""

    def __init__(self, value):
        self.name = value
        self.letter = value[0]

    def report(self):
        """Just prints the name"""
        print(self.name + "\n" + self.letter)

a = MyClass("Anna")
b = MyClass("Bob")

a.report()
b.report()

if type(a) == MyClass:
    print("success")

"""A simple class heirarchy demonstrating inheritance"""
class Animals(object):
    def __init__(self, name, legs):
        self.name = name
        self.legs = legs

class People(Animals):
    def shout(self, words):
        return words.upper()

class Dogs(Animals):
    def __init__(self, name, legs, fc):
        self.furColour = fc
        super().__init__(name, legs)
    def shout(self, words):
        return "Woof"

class Men(People):
    pass

class Women(People):
    pass

bob = Men("Bob", 2)
alice = Women("Alice",2)
fido = Dogs("Fido",4, "Brown")
print(bob.shout("Hello"))
print(alice.shout("Ooops"))
print(fido.shout("Hello"))
print(fido.furColour)

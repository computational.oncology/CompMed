"""A file to demonstrate a simple BP class, demonstrating type-checking and slots"""
import time as t

class BP(object):
    """A simple BP representation"""
    def __init__(self, patient, SBP, DBP):
        self.patient = patient
        self.sys = int(SBP)
        self.dia = int(DBP)
        self.map = round(((2 * self.dia) + self.sys)/3, 1)
        self.time = t.time()
        self.approxTime = round(self.time, 0)

    def report(self):
        """Just prints the name and the MAP"""
        print(self.patient + "\n" + str(self.map) + "\n")

a = BP("Bob", 120, 80)
b = BP("Alice", 140, 90)
a.report()
b.report()

print(a.map)
print(a.approxTime)

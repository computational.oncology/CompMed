"""A file to demonstrate a simple BP class, demonstrating type-checking and slots"""
class BP(object):
    """A simple BP representation"""
    def __init__(self, patient, SBP, DBP):
        self.patient = patient
        self.sys = SBP
        self.dia = DBP
        self.map = (2 * self.dia + self.sys)/3

    def report(self):
        """Just prints the name and the MAP"""
        print(self.patient + "\n" + str(self.map) + "\n")

#Some BPs
a1 = BP("Bob", 120, 80)
a2 = BP("Bob", 120, 80)
b = BP("Alice", 140, 90)
#Some Battleship shots
p1 = ("Bob", 10, 20)
p2 = ("Alice", 5, 17)

def BPAdd(a,b):
    if type(a) == type(b) == BP:
        if a.patient == b.patient:
            sys = (a.sys + b.sys)/2
            dia = (a.dia + b.dia)/2
            return BP(a.patient, sys, dia)
        else: raise ValueError
    else: raise TypeError

c = BPAdd(a1,a2)
c.report()

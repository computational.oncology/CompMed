"""A file to demonstrate a simple BP class, demonstrating type-checking and slots"""
class BP(object):
    """A simple BP representation"""
    def __init__(self, patient, SBP, DBP):
        self.patient = patient
        self.sys = SBP
        self.dia = DBP
        

    def report(self):
        """Just prints the name and the MAP"""
        print(self.patient + "\n" + str(self.sys) + str(self.dia) "\n")

a = BP("Bob", 120, 80)
b = BP("Alice", 140, 90)
a.report()
b.report()

if type(a) == MyClass:
    print("success")
